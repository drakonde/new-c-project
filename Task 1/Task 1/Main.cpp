#include "Record.h"

int main() {
	cout << "Task 1. Implementation of the accounting statement" << endl;
	string past = "statement.txt", text, team;
	fstream file(past, std::ios::in | std::ios::out | std::ios::app);

	if (!file.is_open()) {
		cout << "File on is open!";
		abort();
	}

	while(true) {
		cout << "Enter \"add\" to write to the file, \"list\" to read and \"no\" to exit:" << endl;
		cin >> team;
		cin.sync();
		cin.ignore();
		if (team == "add") {
			std::getline(std::cin, text);
			record list(text);
			//list.print();
			list.list_file(file);
		}
		else if (team == "list") {
			add_file(file);
		}
		else if (team == "no" || team == "No") {
			break;
		}
		else {
			cout << "Unknown command" << endl;
		}
	}

	file.close();
	return 0;
}