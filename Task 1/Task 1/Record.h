#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#define TAB std::setw(10) << std::left
#define TAB2 std::setw(5) << std::left

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::fstream;

struct record {
private:
	string name;
	string surname;
	int day;
	int month;
	int year;
	double sum;
public:
	record(std::string str) {
		std::stringstream ss;
		string date;
		ss << str;
		ss >> name >> surname >> date >> sum;
		day = std::stoi(date.substr(0, 2));
		month = std::stoi(date.substr(3, 2));
		year = std::stoi(date.substr(6));
	}
	void print() {
		cout << "name - " << name << "\t surname - " << surname << "\t day - " << day << "\t month - " << month << "\t year - " << year << "\t sum - " << sum << endl;
	}
	void list_file(std::fstream& file) {
		file << TAB << name << TAB << surname << TAB2 << day << TAB2 << month << TAB << year << sum << endl;
	}
};

void add_file(std::fstream& file) {
	string text;
	file.seekg(0, std::ios_base::beg);
	while (getline(file, text)) {
		cout << text << endl;
	}
}