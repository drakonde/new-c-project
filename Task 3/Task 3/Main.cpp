#include "Header.h"

int main() {
	cout << "Task 3. Implementation of the mathematical vector\n\n" <<
		"Adding two vectors is the add command - 1\nSubtracting two vectors is the subtract command - 2\n" <<
		"Multiplying a vector by a scalar is the scale command - 3\n" <<
		"Finding the length of the vector is the length command - 4\n" <<
		"Normalization of the vector is the normalize command - 5\n\n" << "Enter number command" << endl;
	int num;
	cin >> num;
	switch (num) {
	case 1:
	{
		cout << "Enter coordinates of vectors:" << endl;
		double oneX, oneY, twoX, twoY;
		cin >> oneX >> oneY >> twoX >> twoY;
		vector vec1 = { oneX, oneY }, vec2 = { twoX, twoY };
		cout << "Summa vec1 and vec2 = " << add(vec1, vec2);
		break;
	}
	case 2:
	{
		cout << "Enter coordinates of vectors:" << endl;
		double oneX, oneY, twoX, twoY;
		cin >> oneX >> oneY >> twoX >> twoY;
		vector vec1 = { oneX, oneY }, vec2 = { twoX, twoY };
		cout << "Vector difference = " << subtract(vec1, vec2);
		break;
	}
	case 3:
	{
		cout << "Enter coordinates of vector and scale " << endl;
		double oneX, oneY, scal;
		cin >> oneX >> oneY >> scal;
		vector vec = { oneX, oneY };
		cout << "The product of a vector and a number =" << scale(vec, scal);
		break;
	}
	case 4: {
		cout << "Enter coordinates of vector and scale " << endl;
		double oneX, oneY;
		cin >> oneX >> oneY;
		vector vec = { oneX, oneY };
		cout << "Vector length = " << vec.length();
		break;
	}
	case 5:
	{
		cout << "Enter coordinates of vector and scale " << endl;
		double oneX, oneY{};
		cin >> oneX >> oneY;
		vector vec = { oneX, oneY };
		normalize(vec);
		cout << "Normalized vector = " << vec.x << " " << vec.y;
		break;
	}
	default:
		cout << "Erorr. " << endl;
	}

	return 0;
}