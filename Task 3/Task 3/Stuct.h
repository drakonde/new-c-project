#pragma once
#include "Header.h"

struct vector {
	double x = 0, y = 0;
	double length() {
		return sqrt((pow(x, 2)) + (pow(y, 2)));
	}
	/*
	vector &operator+(vector &two) {
		vector one;
		one.length() + two.length();
		return one;
	}
	*/
};

double add(vector one, vector two) {
	return one.length() + two.length();
}
double subtract(vector one, vector two) {
	return one.length() - two.length();
}
double scale(vector vec, double num){
	return vec.length() * num;
}
void normalize(vector &vec) {
	vec.x /= vec.length();
	vec.y /= vec.length();
}