#include "Header.h"

int main() {
	extern char map[40][40];
	cout << "Task 4. Implementation of a turn-based role-playing game" << endl;
	c_map();
	mob alien[5];
	srand(time(nullptr));
	for (int i = 0; i < 5; ++i) {
		alien[i].name = "Enemy#" + std::to_string(i);
		alien[i].life = (rand() % 100) + 51;
		alien[i].armor = rand() % 51;
		alien[i].damage = (rand() % 15) + 16;
		alien[i].corX = rand() % 40;
		alien[i].corY = rand() % 40;
		map[alien[i].corX][alien[i].corY] = 'E';
	}

	cout << "Enter: name life armor damage" << endl;
	mob player;
	cin >> player.name >> player.life >> player.armor >> player.damage;
	player.print_g();
	
	while (true) {
		player.corX = rand() % 40;
		player.corY = rand() % 40;
		if (map[player.corX][player.corY] == '.') {
			map[player.corX][player.corY] = 'P';
			break;
		}
	}
	bool victori = false;
	while (!victori) {

		print_map();
		for (int i = 0; i < 5; ++i) {
			alien[i].print_g();
		}
		player.print_g();

		cout << "Enter: left, right, top, bottom" << endl;
		string step;
		cin >> step;
		if (step == "bottom" && player.corX != 39) {
			if (map[player.corX + 1][player.corY] == '.') {
				map[player.corX][player.corY] = '.';
				player.corX++;
				map[player.corX][player.corY] = 'P';
			}
			else if (map[player.corX + 1][player.corY] == 'E') {
				map[player.corX][player.corY] = '.';
				player.corX++;
				for (int i = 0; i < 5; ++i) {
					if (alien[i] == player) {
						battle(alien[i], player);
					}
				}
			}
		}
		else if (step == "top" && player.corX != 0) {
			if (map[player.corX - 1][player.corY] == '.') {
				map[player.corX][player.corY] = '.';
				player.corX--;
				map[player.corX][player.corY] = 'P';
			}
			else if (map[player.corX - 1][player.corY] == 'E') {
				map[player.corX][player.corY] = '.';
				player.corX--;
				for (int i = 0; i < 5; ++i) {
					if (alien[i] == player) {
						battle(alien[i], player);
					}
				}
			}
		}
		else if (step == "right" && player.corY != 39) {
			if (map[player.corX][player.corY + 1] == '.') {
				map[player.corX][player.corY] = '.';
				player.corY++;
				map[player.corX][player.corY] = 'P';
			}
			else if (map[player.corX][player.corY + 1] == 'E') {
				map[player.corX][player.corY] = '.';
				player.corY++;
				for (int i = 0; i < 5; ++i) {
					if (alien[i] == player) {
						battle(alien[i], player);
					}
				}
			}
		}
		else if (step == "left" && player.corY != 39) {
			if (map[player.corX][player.corY - 1] == '.') {
				map[player.corX][player.corY] = '.';
				player.corY--;
				map[player.corX][player.corY] = 'P';
			}
			else if (map[player.corX][player.corY - 1] == 'E') {
				map[player.corX][player.corY] = '.';
				player.corY--;
				for (int i = 0; i < 5; ++i) {
					if (alien[i] == player) {
						battle(alien[i], player);
					}
				}
			}
		}
		else if (step == "save") {
			{
				cout << "Save of game" << endl;
				std::ofstream game("game.txt");
				if (!game.is_open()) {
					cout << "File no is open!";
					return 1;
				}
				
				game << player.name << " " << player.armor << " " << player.life << " " << player.damage << " " << player.corX << " " << player.corY << endl;
				for (int i = 0; i < 5; ++i) {
					game << alien[i].name << " " << alien[i].armor << " " << alien[i].life << " " << alien[i].damage << " " << alien[i].corX << " " << alien[i].corY << endl;
				}
				game << mob::count << endl;
				game.write(*map, sizeof(map));
				game.close();
				continue;
				
			}
		}
		else if (step == "load") {
			cout << "Long of game" << endl;
			std::ifstream game("game.txt");
			if (!game.is_open()) {
				cout << "File no is open!";
				return 1;
			}
			
			game >> player.name >> player.armor >> player.life >> player.damage >> player.corX >> player.corY;

			for (int i = 0; i < 5; ++i) {
				game >> alien[i].name >> alien[i].armor >> alien[i].life >> alien[i].damage >> alien[i].corX >> alien[i].corY;
			}
			game >> mob::count;
			while (game.read(*map, sizeof(map)));
			
			game.close();

			continue;
		}
		else {
			cout << "Erorr" << endl;
			continue;
		}

		for (int i = 0; i < 5; ++i) {
			mob_motion(alien[i]);
		}
		
		if (mob::count == 0) {
			victori = true;
		}
		else if (player.life <= 0) {
			break;
		}
	}
	if (victori) {
		cout << "You have won" << endl;
	}
	else {
		cout << "You've lost" << endl;
	}

	return 0;
}