#pragma once
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <cstdlib>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::string;

void c_map();
void print_map();

struct mob {
	string name;
	int life = 0;
	int armor = 0;
	int damage = 0;
	int corX = 0;
	int corY = 0;
	mob(){}
	mob(string name, int life, int armor, int damage) {
		this->name = name;
		this->life = life;
		this->armor = armor;
		this->damage = damage;
	}
	friend void mob_motion(mob& alien);
	void print_g() {
		cout << endl << name << " " << life << " " << armor << " " << damage << endl;
	}
	friend void battle(mob &alien, mob &player);
	bool operator ==(const mob &other) {
		return this->corX == other.corX && this->corY == other.corY;
	}
	mob operator = (mob &other){
		name = other.name;
		life = other.life;
		armor = other.armor;
		damage = other.damage;
		corX = other.corX;
		corY = other.corY;
		return *this;
	}
	static int count;
};
