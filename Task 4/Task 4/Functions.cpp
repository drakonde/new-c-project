#include "Header.h"

const int m_siz = 40;
char map[m_siz][m_siz];
int mob::count = 5;

void c_map(){
	for (int i = 0; i < m_siz; ++i) {
		for (int c = 0; c < 40; ++c) {
			map[i][c] = '.';
		}
	}
}

void print_map() {
	cout << "\n" << std::setfill('=') << std::setw(80) << "=\n";
	for (int i = 0; i < m_siz; ++i) {
		for (int c = 0; c < 40; ++c) {
			cout << map[i][c] << " ";
		}
		cout << endl;
	}
	cout << std::setfill('=') << std::setw(80) << "=\n";
}

void mob_motion(mob& alien) {
	int step = 0;
	if (alien.life > 0) {
		step = rand() % 4;
		if (step == 0) {
			if (alien.corX != 39) {
				if (map[alien.corX + 1][alien.corY] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corX++;
					map[alien.corX][alien.corY] = 'E';
				}
				else if (map[alien.corX + 1][alien.corY] == 'P') {

				}
			}
			else {
				if (map[alien.corX - 1][alien.corY] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corX--;
					map[alien.corX][alien.corY] = 'E';
				}
			}
		}
		else if (step == 1) {
			if (alien.corX != 0) {
				if (map[alien.corX - 1][alien.corY] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corX--;
					map[alien.corX][alien.corY] = 'E';
				}
			}
			else {
				if (map[alien.corX + 1][alien.corY] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corX++;
					map[alien.corX][alien.corY] = 'E';
				}
			}
		}
		else if (step == 2) {
			if (alien.corY != 39) {
				if (map[alien.corX][alien.corY + 1] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corY++;
					map[alien.corX][alien.corY] = 'E';
				}
			}
			else {
				if (map[alien.corX][alien.corY - 1] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corY--;
					map[alien.corX][alien.corY] = 'E';
				}
			}
		}
		else if (step == 3) {
			if (alien.corY != 0) {
				if (map[alien.corX][alien.corY - 1] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corY--;
					map[alien.corX][alien.corY] = 'E';
				}
			}
			else {
				if (map[alien.corX][alien.corY + 1] == '.') {
					map[alien.corX][alien.corY] = '.';
					alien.corY++;
					map[alien.corX][alien.corY] = 'E';
				}
			}
		}
	}
}

void battle(mob& alien, mob& player) {
	player.armor -= alien.damage;
	if (player.armor < 0) {
		player.life += player.armor;
		if (player.life <= 0) {
			map[player.corX][player.corY] = '.';
			map[alien.corX][alien.corY] = 'E';
		}
	}
	alien.armor -= player.damage;
	if (alien.armor < 0) {
		alien.life += alien.armor;
		if (alien.life <= 0) {
			map[alien.corX][alien.corY] = '.';
			map[player.corX][player.corY] = 'P';
			mob::count--;
		}
	}
}